def process_kindle_file(file_path, userinfo=''):
    from bs4 import BeautifulSoup
    import pandas as pd

    pd.set_option('display.max_rows', None)

    soup = BeautifulSoup(open(file_path, encoding='utf-8'), features='html.parser')
    # 解析数据
    # 公共数据
    book_name = soup.find('div', {'class': "bookTitle"}).text.strip()
    author = soup.find('div', {'class': "authors"}).text.strip()
    # print(book_name,author)
    # 标记数据
    list_content = soup.find_all('div',{'class':"noteText"})
    list_head = soup.find_all('h3',{'class':'noteHeading'})

    content_list = zip(list_content,list_head)
    print(content_list)


process_kindle_file(r'C:\Users\sheep431\Documents\book.html')
